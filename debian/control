Source: liblaxjson
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 12),
Standards-Version: 4.4.1
Section: libs
Homepage: https://github.com/andrewrk/liblaxjson
Vcs-Git: https://salsa.debian.org/debian/liblaxjson.git
Vcs-Browser: https://salsa.debian.org/debian/liblaxjson

Package: liblaxjson-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 liblaxjson1 (= ${binary:Version}),
 ${misc:Depends},
Description: relaxed streaming JSON parser library (development files)
 Official JSON is almost human-readable and human-writable. Disabling a few of
 the strict rules makes it significantly more so.
 .
 This library is intended for parsing user input, such as a config file. It is
 not intended for serializing or deserializing, or as a format for computer-to-
 computer communication.
 .
 This relaxed streaming JSON parser allows:
  * unquoted keys
  * single quotes `'`
  * `//` and `/* */` style comments
  * extra commas `,` in arrays and objects
 .
 This package contains the development files.

Package: liblaxjson1
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: relaxed streaming JSON parser library
 Official JSON is almost human-readable and human-writable. Disabling a few of
 the strict rules makes it significantly more so.
 .
 This library is intended for parsing user input, such as a config file. It is
 not intended for serializing or deserializing, or as a format for computer-to-
 computer communication.
 .
 This relaxed streaming JSON parser allows:
  * unquoted keys
  * single quotes `'`
  * `//` and `/* */` style comments
  * extra commas `,` in arrays and objects
